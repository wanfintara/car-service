@extends('crudbooster::admin_template')

@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="box box-default" style="padding-top: 20px;">
         <div class="box-body table-responsive no-padding">
            <div class="col-md-12">
               <table class="table table-striped table-bordered" id="table-master">
                  <thead>
                     <tr class="info">
                        <th>ID</th>
                        <th>Lokasi</th>
                        <th>Service</th>
                        <th>Cost / Service</th>
                        <th>Cost / Day</th>
                        <th>Cost / Half Day</th>
                        <th>Cost / Hour</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>

</div>
@endsection

@push('bottom')
<script src="{{ asset('js/yajrabox.dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('js/yajrabox.dataTables.min.js') }}"></script>
<script src="{{ asset('js/yajrabox.handlebars.js') }}"></script>
<script type="text/javascript">
   $(function() {
      dt_master();
   });

   function dt_master() {
      $('#table-master').DataTable({
         processing: true,
         serverside: true,
         ajax: "{{ route('guide.dt.master') }}",
         columns: [
            {data: 'id'},
            {data: 'location'},
            {data: 'name_service'},
            {data: 'cost_per_service'},
            {data: 'cost_per_day'},
            {data: 'cost_per_half_day'},
            {data: 'cost_per_hour'}
         ]
      });
   }
</script>
@endpush
