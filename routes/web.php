<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/cms_users/dt_master', ['as' => 'user.dt.master', 'uses' => 'AdminCmsUsers1Controller@dt_master']);

Route::get('/admin/services/dt_master', ['as' => 'service.dt.master', 'uses' => 'AdminServicesController@dt_master']);

Route::get('/admin/guides/dt_master', ['as' => 'guide.dt.master', 'uses' => 'AdminGuidesController@dt_master']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
